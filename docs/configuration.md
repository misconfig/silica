# Silica - Salt configuration

## Configure SaltStack with Silica
install Salt OS packges (ignore if Salt Master, Minion and API is already installed in your environment)

```
yum -y install salt-master salt-minion salt-api
```

### create cert
```
openssl genrsa -out /etc/ssl/certs/saltapi_key.pem 4096
openssl req -new -x509 -key /etc/ssl/certs/saltapi_key.pem -out /etc/ssl/certs/saltapi_cert.pem -days 1826

hit Enter for every prompt, this will generate the Cert
```

### configure Salt API

vim /etc/salt/master.d/salt-api.conf
```
rest_cherrypy:
  port: 9709
  ssl_crt: /etc/ssl/certs/saltapi_cert.pem
  ssl_key: /etc/ssl/certs/saltapi_key.pem
```

### configure authentication
vim /etc/salt/master.d/auth.conf
```
external_auth:
  pam:
    silica:
      - .* 
```

restart Salt Master
```
systemctl restart salt-master
```

Optional: add read/write to Master log for Silica user
```
root@master> setfacl -Rm u:silica:rwx /var/log/salt
```


### start Salt-API
``` 
systemctl start salt-api.service
```

### Test API usage

get token
```
 curl -ski https://localhost:9709/login -H 'Accept: application/json' -d username='silica' -d password='silica_password' -d eauth='pam'

 generates token: bddae5de1962a42ef26dbbf5a406eb528524413f
```


make a call
```
curl -sk https://localhost:9709/minions -H 'Accept: application/json' -H 'X-Auth-Token: bddae5de1962a42ef26dbbf5a406eb528524413f' -d client='local' -d tgt='minion01' -d fun='test.ping'
```