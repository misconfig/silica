# Troubleshooting Silica

## can't execute remote commands
make sure you can execute anything from your salt master using the 'silica' user

```
root@master> su silica
silica@master> salt -a pam \* test.ping
username: silica
password: 
minion1:
    True
```
