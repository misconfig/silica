# Silica Installation

The following shows a setup using Centos7 and Python 3.6.7

### Prerequisite packages
install prerequisite packages
```
yum install gcc openssl-devel bzip2-devel libffi libffi-devel
```

### Python 3.6
install Python 3.6 (as root or sudo)
```
yum install -y https://centos7.iuscommunity.org/ius-release.rpm
yum update
yum install -y python36u python36u-libs python36u-devel python36u-pip
```
check your python 3.6 version,
```
> python3.6 -V
Python 3.6.7
```

### Pipenv
install Pipenv using pip3.6
```
pip3.6 install pipenv 
```

## install Silica
1. on your Salt Master host, create a new Silica service user
    ```
    useradd silica
    passwd silica
    <enter your password>
    ```
2. clone this repo
    ```
    cd /home/silica
    git clone git@gitlab.com:perfecto25/silica.git
    ```
3. update your Python version in Pipfile 
    ```
    [requires]
    python_version = "3.6"

    [scripts]
    silica = 'python3.6 app.py'
    ```
4. install Silica 
    ```
    pipenv install
    ```
    
5. configure salt-api (see Configure section)

## Run Silica
```
pipenv run silica
```
---



