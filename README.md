# SILICA
version: 2018.3.4-001

A Salt management console based on Flask web framework

## minimum requirements
- python version: 3.6
- pip packages: *pip, pipenv*
- salt version 2018.3+ *(see instructions below on how to install Salt)*



---
## Additional
Bootstrap version:


Theme:  https://startbootstrap.com/themes/sb-admin-2/