#!/bin/bash
# SILICA Install and Config (initd systems only!)

# MANUAL STEPS REQUIRED:
# 1. install nginx
# 2. install python-pip

function check_OS {

    if [ -f /etc/os-release ]
    then
        os_file="/etc/os-release"
        id=$(grep "^ID=" $os_file | awk -F= '{print $2}' | awk '{print tolower($0)}')
        release=$(grep "VERSION_ID=" $os_file | awk -F= '{print $2}' | tr -d '"')
    else
        echo "cannot determine OS, exiting installer"
        exit 1
    fi

    if [ $id == "ubuntu" ]
    then
        if [[ ! $release =~ ^(18.04|16.04)$ ]]
        then 
            echo "OS is not compatible Ubuntu version, exiting"
            exit 1
        fi

    elif [ $id == "centos" ]
    then
        if [[ ! $release =~ ^(7.5|7.6)$ ]]
        then 
            echo "OS is not compatible Centos version, exiting"
            exit 1
        fi

    else
        echo "OS not compatible with installer, exiting"
        exit 1
    fi 
}

function install_pkg {
    if [ $1 == 'ubuntu' ]
    then
        echo "installing packages"
        apt install openssl-dev 
    elif [ $1 == 'centos' ]
    then 
        echo "yum"
    fi

}

check_OS
echo $id
echo $release
install_pkg $id
# user=ec2-user
# app_dir=/home/$user/oquery

# # Oracle client libs
# sudo mkdir -p /usr/lib/oracle/12.2/client64/lib/
# sudo cp oracle_libs/* /usr/lib/oracle/12.2/client64/lib/



# # VirtualEnv
# cd $app_dir && virtualenv venv
# source $app_dir/venv/bin/activate

# # Python deps
# cd $app_dir/install
# sudo pip install -r requirements.txt

# # startup script
# sudo mkdir -p /var/log/gunicorn
# sudo chown $user:$user /var/log/gunicorn
# sudo cp oquery.service.systemd  /etc/systemd/system/oquery.service
# sudo systemctl enable oquery.service
# sudo systemctl daemon-reload

# #sudo chmod +x /etc/init.d/oquery
# #sudo chkconfig --add oquery
# #sudo chkconfig oquery on
# #sudo service oquery restart

# # NGINX
# sudo cp oquery.nginx.conf /etc/nginx/sites-available/oquery
# sudo ln -s /etc/nginx/sites-available/oquery /etc/nginx/sites-enabled
# sudo service nginx restart
