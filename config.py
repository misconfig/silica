import os
WTF_CSRF_ENABLED = True
SECRET_KEY = 'maytheshwartzbewithyou'

# port on which app runs on
PORT = 5100  

# directory & file for overall logging
BASEDIR = os.getcwd()
LOGDIR = 'logs'
LOGFILE = 'silica.log'  

SALT_API = {
    'user': 'silica',
    'pw': 'silica',
    'host': 'localhost',
    'port': 9709,
    'allowed_users': 'root'
}

ALLOWED_CMDS = {
    'group': ['add', 'delete', 'info', 'adduser', 'deluser'],
    'test': ['ping', 'version', 'versions', 'versions_report', 'echo'],
    'status': ['all_status', 'cpuinfo', 'cpustats', 'diskusage'],
    'cmd': ['run'],
    'service': ['get_all'],
    'grains': ['items', 'ls', 'get'],
    'user': ['list_users', 'add', 'delete', 'getent', 'info', 'list_groups', 'rename', 'chgid', 'chgroups'],
    'state': ['highstate']
}

# commands that cannot run test=true
NO_TEST = ['test', 'grains', 'service.get_all', 'status', 'user.info', 'user.list']

