import os
import sys
from quart import Quart, websocket
import quart.flask_patch
from quart import Quart
from flask_bootstrap import Bootstrap
from loguru import logger
from config import BASEDIR

LOGDIR = BASEDIR + '/logs' 

if not os.path.exists(LOGDIR):
    os.makedirs(LOGDIR)

logger.add(LOGDIR + '/silica.log', 
    rotation='25MB', 
    colorize=True, 
    format="<green>{time:YYYYMMDD_HH:mm:ss}</green> | {level} | <level>{message}</level>",
    level="DEBUG")

app = Quart(__name__)
app.config.from_object('config')
Bootstrap(app)
 
# get absolute app_dir
app_dir = os.path.dirname(os.path.abspath(__file__))

from app.views import main, errors, minions, run_cmd
