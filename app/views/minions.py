import json
from dictor import dictor
from quart import render_template
from loguru import logger
from app import app
from app.views.cmd import get_client
from app.views.db import get_minions, get_highstate_totals, get_sync_date

nav = 'minions'

@app.route('/minion_status')
async def minion_status():
    ''' salt minion connection status '''
    minions = get_minions()
    return await render_template("minions.html", title='Silica', sync=get_sync_date(), minions=minions) 


@app.route('/highstate')
async def highstate():
    ''' show highstate details '''
    totals = get_highstate_totals()
    return await render_template("highstate.html", title='Silica - Highstate', totals=totals, sync=get_sync_date()) 