import json
from dictor import dictor
from quart import render_template, flash, jsonify
from flask import request
from loguru import logger
from app import app
from app.views.cmd import get_client
from app.views.db import get_targets
from config import ALLOWED_CMDS

block = __name__.split('.')[-1]
nav = 'run_cmd'
target_list = get_targets()

@app.route('/run_cmd')
async def run_cmd():
    ''' 
    generate a form to enter a target and command
    '''
    return await render_template("run_cmd.html", title='Saltstack - Execute Command', cmd_list=ALLOWED_CMDS, target_list=target_list, nav=nav)

@app.route('/run_cmd_execute', methods=['GET', 'POST'])
async def run_cmd_execute():
    '''
    execute a remote Salt command on target 
    '''

    if request.method == 'POST':

        cmd_name = request.form.get('cmd_name')
        hostname = request.form.get('hostname')
        args = request.form.get('args')
        simulate = request.form.get('simulate')
        output_fmt = request.form.get('output_fmt')

        if not hostname or hostname == '':
            return await render_template('run_cmd.html', title='Saltstack - Error', cmd_list=ALLOWED_CMDS, nav=nav, error_msg='You must provide a hostname', target_list=target_list)
 
        if not cmd_name or cmd_name == '':
            return await render_template('run_cmd.html', title='Saltstack - Error', cmd_list=ALLOWED_CMDS, nav=nav, error_msg='You must provide a command', target_list=target_list)

        client = get_client() 
        result = client.run_cmd(hostname, cmd_name, args, run_async=False)

        result = dictor(result, hostname)
        result = json.dumps(result, indent=2)
        logger.info(f"executing cmd: salt {hostname} {cmd_name} {args}")
        return await render_template('run_cmd.html', title='Saltstack - Cmd Results', action='display_cmd_results', result=result, cmd_list=ALLOWED_CMDS, hostname=hostname, cmd_name=cmd_name, args=args, output_fmt=output_fmt, nav=nav, target_list=target_list)
