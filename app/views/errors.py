
from flask import request
from quart import render_template
from app import app
from loguru import logger


# ERROR HANDLERS
@app.errorhandler(404)
def page_not_found(error):
    logger.error('Page not found: {}', request.path)
    return render_template('404.html', title='404 Error', msg=request.path)

@app.errorhandler(500)
def internal_server_error(error):
    logger.error('Server Error: {}', error)
    return render_template('500.html', title='500 Error', msg=error)

@app.errorhandler(Exception)
def unhandled_exception(e):
    logger.error('Unhandled Exception: {}', str(e))
    return render_template('error.html', title='Exception', msg=str(e)), 500

