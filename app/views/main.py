# VIEW MAIN

import json
from dictor import dictor
from app import app
from quart import render_template
from loguru import logger

@app.route('/')
@app.route('/index')
async def index():
    return await render_template("index.html", title='Silica')

@app.route('/dashboard')
async def dashboard():
    ''' 
    show statistics dashboard 
    '''
    return await render_template("dashboard.html", title='Silica - Dashboard') 


@app.route('/cheatsheet')
async def cheatsheet():
    ''' 
    docs > cheatsheet 
    '''
    return await render_template("cheatsheet.html", title='Silica - Salt Cheatsheet') 