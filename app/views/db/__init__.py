import os
import sys
import json
from dictor import dictor
from loguru import logger
from tinydb import TinyDB, Query
import pendulum
from config import BASEDIR

db_minions = BASEDIR + '/app/views/db/minions.json'
db_highstate = 'highstate.json'

def check_db():
    '''
    checks if a Minion DB has been created
    '''
    if not os.path.exists(db_minions):
        logger.warning('not exist')
        raise Exception('Minion DB file not found, please run Generator to create a new DB')

def get_minions():
    ''' get all minion information '''
    
    check_db()
    
    db = TinyDB(db_minions)

    minions = []

    for value in db.all():
        minions.append(dict(value))
    logger.debug(minions)
    if minions:
        return minions
    else:
        raise Exception('No minion data available. Please run "Update DB" to create data')

def get_targets():
    '''
    returns list of minions that are active/connected
    '''
    #logger.warning(db_minions)
    db = TinyDB(db_minions)
    Q = Query()
    
    targets = []
    for target in db.search(Q.status == 'True'):
        targets.append(target['name'])
    return targets

def get_highstate():
    '''
    returns minion highstates
    '''

    db = TinyDB(db_minions)
    Q = Query()
    targets = []
    for target in db.search(Q.status == 'True'):
        targets.append(target['name'])
    return targets

def get_highstate_totals():
    '''
    gets all minions and their highstate results
    returns totals for Changes, No Changes and Failures
    '''
    totals = {}
    minions = get_minions()

    for _min in minions:
        name = dictor(_min, 'name')

        if name:
            totals[name] = {}
            summary = dictor(_min, 'highstate')

            if summary:
                nochanges, changes, failures = 0,0,0

                for hsitem in summary:
                    if summary[hsitem]['result'] is True:
                        nochanges += 1
                    if summary[hsitem]['result'] is None:
                        changes += 1
                    if summary[hsitem]['result'] is False:
                        failures += 1

                totals[name]['nochanges'] = nochanges
                totals[name]['changes'] = changes
                totals[name]['failures'] = failures
                totals[name]['summary'] = summary
            else:
                totals[name]['summary'] = 'Unavailable'
    return totals

def get_sync_date():
    '''
    returns when DB generator was last run (N minutes ago)
    '''
    
    now = pendulum.now()
    db = TinyDB(db_minions)
    Q = Query()
    sync = db.search(Q.sync)[0]['sync']
    logger.warning(sync)
    if sync:
        sync = pendulum.parse(sync)
        difference = "{:,}".format(now.diff(sync).in_minutes())
        return difference
    else:
        return "No DB data available, run DB Generator to create a new DB file"

