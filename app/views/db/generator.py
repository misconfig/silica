import os
import sys
import json
from dictor import dictor
import pendulum
from pathlib import Path

app_path = Path(__file__).resolve().parents[3]
db_path = Path(__file__).resolve().parents[0]

# append App to get Silica libs
sys.path.append(str(app_path))

from config import SALT_API, BASEDIR, LOGDIR, LOGFILE
from app.views.cmd import get_client
from app.views.db import get_targets
from loguru import logger
from tinydb import TinyDB, Query

if not os.path.exists(LOGDIR):
    os.makedirs(LOGDIR)

logger.add(LOGDIR + '/' + LOGFILE, 
    rotation='25MB', 
    colorize=True, 
    format="<green>{time:YYYYMMDD_HH:mm:ss}</green> | {level} | <level>{message}</level>",
    level="DEBUG")


def sync_date(db, Q):
    ''' timestamps latest sync '''

    now = pendulum.now()
    
    sync_id = dictor(db.all(), '0')
    
    if sync_id:
        db.update({'sync': f'{now}'}, doc_ids=[1])
    else:
        db.insert({'sync': f'{now}'})

def run_cmd(target, cmd, args=None, expr_form=None):
    client = get_client()
    return client.run_cmd(target, cmd, args, run_async=False, expr_form=expr_form)

def get_status(db, Q):
    data = run_cmd('*', 'test.ping')
    for name in data:
        if db.search(Q.name == name):
            db.update({'status': '{}'.format(data[name])}, Q.name == name)   
        else:
            db.insert({'name': '{}'.format(name), 'status': '{}'.format(data[name])})

def get_grains(db, Q):
    '''
    get all available grains from active/connected minions
    '''
    # get all targets that are up and active, skip disconnected minions
    target_list = get_targets()

    if target_list:
        data = run_cmd(target_list, 'grains.items', expr_form='list')
        for name in data:
            if db.search(Q.name == name):
                db.update({'grains': data[name]}, Q.name == name)   
            else:
                db.insert({'name': '{}'.format(name), 'grains': data[name]})
    else:
        logger.warning('No active minions found')

def get_highstate(db, Q):
    '''
    get highstate information for all active minions
    '''
    # get all targets that are up and active, skip disconnected minions
    target_list = get_targets()

    if target_list:

        data = run_cmd(target_list, 'state.highstate', args='test=true', expr_form='list')

        for name in data:
            if db.search(Q.name == name):
                db.update({'highstate': data[name]}, Q.name == name)   
            else:
                db.insert({'name': '{}'.format(name), 'highstate': data[name]})

if __name__ == "__main__":
    
    minions_db = str(db_path) + '/minions.json'

    if not os.path.exists(minions_db):
        os.mknod(minions_db)

    db = TinyDB(minions_db)
    Q = Query()
    
    sync_date(db, Q)
    get_status(db, Q)
    get_grains(db, Q)
    get_highstate(db, Q)