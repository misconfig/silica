# BLOCK Saltstack

import os
import sys
import yaml
import json
import requests
from dictor import dictor
from loguru import logger
#from config import SALT_API
from .client import SaltClient  
#from app.blocks.common import get_config

def get_client():
    ''' returns SaltAPI connection  '''
    client = SaltClient()
    client.login()
    return client

