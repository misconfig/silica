import requests
import json
from loguru import logger
from dictor import dictor
from config import SALT_API

class SaltClient(object):
    def __init__(self, **kwargs):
        
        username = dictor(SALT_API, 'user', checknone=True)
        password = dictor(SALT_API, 'pw', checknone=True)
        host = dictor(SALT_API, 'host', checknone=True)
        port = dictor(SALT_API, 'port', checknone=True)
        logger.debug(username)
        self.token = None
        self.host = host
        self.port = port
        self.username = username
        self.password = password
        self.verify_ssl_cert = kwargs.get("verify_ssl_cert", False)

    def login(self):
        url = 'https://' + self.host  + ':' + str(self.port) + '/login'
        req = requests.post(url, data={'eauth': 'pam', 'username': self.username, 'password': self.password}, headers={'Accept': 'application/json'}, verify=self.verify_ssl_cert, timeout=5.0)

        if req.status_code != 200:
            raise ConnectionException("Signing in to salt (%s) failed with status code %s (body %s)" % (self.salt_host, req.status_code, req.text))
        #slogger.debug("Salt login response: %s - %s", req.status_code, req.text)

        if req.status_code != 200:
            raise AuthenticationException("Signing in to salt failed.")
        resp = req.json()
        #logger.debug(resp)
        self.token = dictor(resp, 'return.0.token', checknone=True)
        return self.token

    def _get_headers(self):
        return {"Accept": "application/json", "X-Auth-Token": self.token}

    def run_cmd(self, minion_id, cmd_name, args=None, run_async=False, expr_form=None):
        client = 'local_async' if run_async else 'local'
        url = 'https://' + self.host  + ':' + str(self.port)
        
        #if args:            
        #minion_id = 'saltmaster,min2'
        data = {}
        data['client'] = client
        data['tgt'] = minion_id
        data['fun'] = cmd_name
        if args:
            data['arg'] = args + ' test=true'
        if expr_form:
            data['expr_form'] = expr_form
        
        logger.debug(data)
        
        req = requests.post(url, data=data, headers=self._get_headers(), verify=self.verify_ssl_cert)
        resp = req.json()
        logger.warning(resp)
        data = dictor(resp, 'return.0', checknone=True)
        #logger.debug(data)
        return data

    def run_async_cmd(self, minion_id, cmd_name, args=None):
        ''' runs Salt command asyncronously '''
        url = 'https://' + self.host  + ':' + str(self.port)

        if args:
            data = {"client": "local_async", "tgt": minion_id, "fun": cmd_name, "arg": args}
        else:
            data = {"client": "local_async", "tgt": minion_id, "fun": cmd_name}

        req = requests.post(url, data=data, headers=self._get_headers(), verify=self.verify_ssl_cert)
        resp = req.json()
        data = dictor(resp, 'return.0.jid', checknone=True)
        logger.debug(data)
        return json.dumps(data)

    def check_job_status(self, job_id):
        resp = requests.post(self.salt_host, data={"client": "runner", "fun": "jobs.lookup_jid", "jid": job_id}, headers=self._get_headers(), verify=self.verify_ssl_cert)
        logger.debug("lookup_jid response: %s - %s", resp.status_code, resp.text)
        data = resp.json()
        return None

    def get_minions(self):
        url = 'https://' + self.host  + ':' + str(self.port)
        req = requests.get(url+'/minions', headers=self._get_headers(), verify=self.verify_ssl_cert)

    # def run_command(self, target, command, args):
    #     resp = requests.post(self.salt_host, data={"client": "local", "tgt": target, "fun": command, "arg": args}, headers=self._get_headers(), verify=self.verify_ssl_cert)
    #     logger.debug("Salt cmd response: %s - %s", resp.status_code, resp.text)
    #     data = resp.json()

    #     machines = data["return"][0]
    #     for machine, machine_data in machines.items():
    #         for _, command_data in machine_data.items():
    #             if not command_data["result"]:
    #                 return False
    #     return True