
import os
import sys
import yaml
import json
import requests
from dictor import dictor
from loguru import logger
import datetime
from app.views.common import get_config
from app.views.cmd import get_client
#from app.blocks.cmd.db import db_conn


def get_api_token():
    creds = get_config('salt_api')
    user = dictor(creds, 'user', checknone=True)
    pw = dictor(creds, 'pw', checknone=True)
    host = dictor(creds, 'host', checknone=True)
    port = dictor(creds, 'port', checknone=True)

    url = 'https://' + host + ':' + str(port) + '/login'
    session = requests.Session()
    session.verify = False
    resp = session.post(url, json={'username': user, 'password': pw, 'eauth': 'pam'})
    data = resp.json()
    token = dictor(data, 'return.0.token', checknone=True)
    return token

# def minion_status():
#     ''' inserts minion status into TinyDB '''
    
#     timestamp = datetime.datetime.now()
#     logger.warning(timestamp)
    
#     db = db_conn()
#     client = get_client() 
#     all_minions = client.run_cmd('*', 'test.ping')
#     logger.warning(type(all_minions))

#     # delete previous entries
#     db.purge_table('minion_status')
#     table = db.table('minion_status')
#     table.insert({'date': str(timestamp), 'status': all_minions})

# def minion_grains():
#     ''' inserts minion grain info into TinyDB '''
    
#     timestamp = datetime.datetime.now()
#     logger.warning(timestamp)
    
#     db = db_conn()
#     client = get_client() 
#     all_minions = client.run_cmd('*', 'test.ping')
#     logger.warning(type(all_minions))

#     # delete previous entries
#     db.purge_table('minion_status')
#     table = db.table('minion_status')
#     table.insert({'date': str(timestamp), 'status': all_minions})
